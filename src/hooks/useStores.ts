import { appStores } from "../stores";

type TParams = keyof typeof appStores;

export const useStores = (keyStore: TParams) => {
  return appStores[keyStore];
};
