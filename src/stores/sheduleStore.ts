import { action, makeObservable, observable } from "mobx";
import { v4 as uuidv4 } from "uuid";
import { Doctors } from "../pages/mockDoctors";
import { TDoctor, TSheduleItem } from "../types/shedule";

class SheduleStore {
  @observable list: TSheduleItem[] = [];
  @observable doctors: TDoctor[] = [];

  constructor() {
    makeObservable(this);
    this.doctors = this.fetchDoctorsList();
  }

  @action
  private fetchDoctorsList() {
    return Doctors.map((name) => ({
      id: uuidv4(),
      name,
    }));
  }

  isAvaliable(date: Date) {
    return this.list.every((v) => v.date.getTime() !== date.getTime());
  }

  @action
  createAppointment(data: TSheduleItem) {
    if (this.isAvaliable(data.date)) {
      this.list.push(data);
      return true;
    }
    return false;
  }

  @action
  deleteAppointment(recordId: string) {
    this.list = this.list.filter((v) => v.id !== recordId);
  }

  getAppointments(doctorId: string | null, date: Date | null) {
    if (!doctorId || !date) {
      return [];
    }

    const queryDate = date.getDate();
    return this.list.filter((item) => {
      const dateCond = item.date.getDate() === queryDate;
      const idCond = item.doctorId === doctorId;
      return idCond && dateCond;
    });
  }

  getDoctorInfo(doctorId: string) {
    return this.doctors.find((v) => v.id === doctorId);
  }
}

export const sheduleStore = new SheduleStore();
