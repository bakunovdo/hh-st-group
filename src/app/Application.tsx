import { Container, makeStyles, Paper } from "@material-ui/core";
import { createBrowserHistory } from "history";
import { Route, Router, Switch } from "react-router-dom";
import { Header } from "../components/header";
import { Routes } from "../enums/routes";
import { Form } from "../pages/Form";
import { Shedule } from "../pages/Shedule";

const browserHistory = createBrowserHistory();

const useStyles = makeStyles(() => ({
  paper: {
    marginTop: "2rem",
    padding: "2rem",
  },
}));

export const Application = () => {
  const styles = useStyles();

  return (
    <Router history={browserHistory}>
      <Header />
      <Switch>
        <Container maxWidth="lg">
          <Paper className={styles.paper} elevation={2}>
            <Route exact path={Routes.pHome} component={Form} />
            <Route exact path={Routes.pForm} component={Form} />
            <Route exact path={Routes.pShedule} component={Shedule} />
          </Paper>
        </Container>
      </Switch>
    </Router>
  );
};
