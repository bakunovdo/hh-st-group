import DateFnsUtils from "@date-io/date-fns";
import {
  Box,
  Button,
  makeStyles,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { v4 as uuidv4 } from "uuid";
import { useStores } from "../hooks/useStores";
import { TSheduleItem } from "../types/shedule";
import { getDate30 } from "../utils/date";

const useStyles = makeStyles((theme) => ({
  form: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  select: {
    minWidth: "200px",
    marginLeft: "1rem",
  },
}));

export const Form = () => {
  const styles = useStyles();

  const { control, handleSubmit } = useForm();

  const sheduleStore = useStores("sheduleStore");

  const [status, setStatus] = useState<boolean>();

  const onSubmit = (data: Omit<TSheduleItem, "id">) => {
    const status = sheduleStore.createAppointment({
      ...data,
      id: uuidv4(),
    });

    setStatus(status);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Box>
          <Typography variant="subtitle1">ФИО Пациента</Typography>
        </Box>
        <Box className={styles.form}>
          <Controller
            name="firstName"
            control={control}
            rules={{ required: true }}
            render={({ field }) => <TextField required label="Имя" variant="outlined" {...field} />}
          />

          <Controller
            name="lastName"
            control={control}
            rules={{ required: true }}
            render={({ field }) => (
              <TextField required label="Фамилия" variant="outlined" {...field} />
            )}
          />

          <Controller
            name="patronymic"
            control={control}
            rules={{ required: true }}
            render={({ field }) => (
              <TextField required label="Отчество" variant="outlined" {...field} />
            )}
          />
        </Box>
        <Box display="flex" mt="1rem">
          <Typography variant="subtitle1">Выберите врача</Typography>
          <Controller
            name="doctorId"
            control={control}
            rules={{ required: true }}
            render={({ field }) => (
              <Select required className={styles.select} {...field}>
                {sheduleStore.doctors.map(({ id, name }) => (
                  <MenuItem key={id} value={id}>
                    {name}
                  </MenuItem>
                ))}
              </Select>
            )}
          />
        </Box>
        <Box display="flex" mt="2rem">
          <Typography variant="subtitle1">Выберите время</Typography>
          <Box ml="1rem">
            <Controller
              name="date"
              control={control}
              defaultValue={getDate30(new Date())}
              rules={{ required: true }}
              render={({ field }) => <DateTimePicker disablePast minutesStep={30} {...field} />}
            />
          </Box>
        </Box>
        <Box mt="2rem" width="100%">
          <Controller
            name="complaints"
            control={control}
            render={({ field }) => (
              <TextField
                fullWidth
                multiline
                rowsMax={4}
                label="Жалобы"
                variant="outlined"
                {...field}
              />
            )}
          />
        </Box>
        {status != null && <Box>{status ? "Запись создана" : "Запись уже существует"}</Box>}

        <Box mt="2rem">
          <Button type="submit" variant="contained" color="primary">
            Подтвердить
          </Button>
        </Box>
      </MuiPickersUtilsProvider>
    </form>
  );
};
