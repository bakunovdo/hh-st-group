import DateFnsUtils from "@date-io/date-fns";
import { Box, Checkbox, makeStyles, MenuItem, Select, Typography } from "@material-ui/core";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { observer } from "mobx-react-lite";
import { ChangeEvent, useState } from "react";
import { SheduleList } from "../components/sheduleList";
import { useStores } from "../hooks/useStores";

const useStyles = makeStyles((theme) => ({
  select: {
    minWidth: "200px",
    marginLeft: "1rem",
  },
}));

export const Shedule = observer(() => {
  const styles = useStyles();
  const sheduleStore = useStores("sheduleStore");

  const [selectedDate, handleDateChange] = useState<Date | null>(new Date());
  const [selectedDoctor, setDoctor] = useState<string | null>(null);
  const [showAll, setShowAll] = useState(false);

  const handleDoctorChange = (event: ChangeEvent<{ value: unknown }>) => {
    if (typeof event.target?.value === "string") {
      setDoctor(event.target.value);
    }
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Box display="flex" alignItems="center">
        <Typography variant="subtitle2">Расписание врачей</Typography>
        <Box display="flex" ml="auto" alignItems="center">
          <Typography variant="subtitle2">Показать все записи</Typography>
          <Checkbox
            checked={showAll}
            onChange={(e) => setShowAll(e.target.checked)}
            inputProps={{ "aria-label": "primary checkbox" }}
          />
        </Box>
      </Box>
      <Box display="flex" alignItems="center" mt="1rem">
        <Typography variant="subtitle1">Выберите врача</Typography>
        <Select
          required
          className={styles.select}
          value={selectedDoctor}
          onChange={handleDoctorChange}
        >
          {sheduleStore.doctors.map(({ id, name }) => (
            <MenuItem key={id} value={id}>
              {name}
            </MenuItem>
          ))}
        </Select>
        <Box ml="1rem">
          <DatePicker
            disableToolbar
            variant="inline"
            label="Выберите дату"
            value={selectedDate}
            onChange={handleDateChange}
          />
        </Box>
      </Box>

      <SheduleList
        items={sheduleStore.getAppointments(selectedDoctor, selectedDate)}
        showDoctorName={showAll}
      />
    </MuiPickersUtilsProvider>
  );
});
