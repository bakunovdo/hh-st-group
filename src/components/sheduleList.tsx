import { Box, Button, Typography } from "@material-ui/core";
import { observer } from "mobx-react-lite";
import { format } from "date-fns";

import { useStores } from "../hooks/useStores";

import { TDoctor, TSheduleItem } from "../types/shedule";

type TItemProps = {
  patient: TSheduleItem;
  doctorInfo?: TDoctor;
  showDoctorName: boolean;
  onDelete: () => void;
};

const Item = (props: TItemProps) => {
  return (
    <Box
      display="flex"
      alignItems="center"
      border="1px solid"
      borderRadius="4px"
      p="0.5rem"
      mt="1rem"
    >
      <Box flexGrow="1">
        <Box display="flex" alignItems="center">
          <Typography variant="subtitle2">ФИО:</Typography>
          <Typography variant="body1">
            {props.patient.lastName} {props.patient.firstName} {props.patient.patronymic}
          </Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <Typography variant="subtitle2">Имя врача:</Typography>
          <Typography variant="body1">{props.doctorInfo?.name}</Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <Typography variant="subtitle2">Жалобы:</Typography>
          <Typography variant="body1">{props.patient.complaints || "Жалоб нет"}</Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <Typography variant="subtitle2">Время записи:</Typography>
          <Typography variant="body1">{format(props.patient.date, "dd.MM.yyyy HH:mm")}</Typography>
        </Box>
      </Box>
      <Box>
        <Button variant="contained" color="secondary" onClick={props.onDelete}>
          Удалить запись
        </Button>
      </Box>
    </Box>
  );
};

type TPropsList = {
  items: TSheduleItem[];
  showDoctorName: boolean;
};

export const SheduleList = observer(({ items, showDoctorName }: TPropsList) => {
  const sheduleList = useStores("sheduleStore");

  if (!items.length) {
    return (
      <Box mt="2rem">
        <Typography variant="subtitle2">Записей нет</Typography>
      </Box>
    );
  }

  return (
    <Box mt="2rem">
      {items.map((props) => {
        const doctor = sheduleList.getDoctorInfo(props.doctorId);
        return (
          <Item
            key={props.id}
            patient={props}
            doctorInfo={doctor}
            showDoctorName={showDoctorName}
            onDelete={() => sheduleList.deleteAppointment(props.id)}
          />
        );
      })}
    </Box>
  );
});
