import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import { Link } from "react-router-dom";
import { Routes } from "../enums/routes";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginBottom: "36px",
  },
}));

export const Header = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Link to={Routes.pHome}>
            <Button color="inherit">Расписание врача</Button>
          </Link>
          <Link to={Routes.pShedule}>
            <Button color="inherit">Записаться к врачу</Button>
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  );
};
