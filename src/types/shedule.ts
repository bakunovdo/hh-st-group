export type TSheduleItem = {
  firstName: string;
  lastName: string;
  patronymic: string;
  doctorId: string;
  date: Date;
  complaints?: string;
  id: string;
};

export type TDoctor = {
  id: string;
  name: string;
};
