export function getDate30(date: Date) {
  const mins = date.getMinutes();
  const hours = date.getHours();

  if (mins >= 30) {
    date.setHours(hours + 1);
    date.setMinutes(0);
  } else {
    date.setMinutes(30);
  }

  date.setSeconds(0);
  date.setMilliseconds(0);

  return date;
}
